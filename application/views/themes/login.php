<!DOCTYPE HTML>
<html>
<head>
<title>PTSP - Kementrian Agama Daerah Istimewa Yogyakarta</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Glance Design Dashboard Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="<?= base_url(); ?>public/admin/css/bootstrap.css" rel='stylesheet' type='text/css' />

<!-- Custom CSS -->
<link href="<?= base_url(); ?>public/admin/css/style.css" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="<?= base_url(); ?>public/admin/css/font-awesome.css" rel="stylesheet">
<!-- //font-awesome icons CSS-->

<!-- side nav css file -->
<link href='<?= base_url(); ?>public/admin/css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
<!-- //side nav css file -->

 <!-- js-->
<script src="<?= base_url(); ?>public/admin/js/jquery-1.11.1.min.js"></script>
<script src="<?= base_url(); ?>public/admin/js/modernizr.custom.js"></script>

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts-->

<!-- chart -->
<script src="<?= base_url(); ?>public/admin/js/Chart.js"></script>
<!-- //chart -->

<!-- Metis Menu -->
<script src="<?= base_url(); ?>public/admin/js/metisMenu.min.js"></script>
<script src="<?= base_url(); ?>public/admin/js/custom.js"></script>
<link href="<?= base_url(); ?>public/admin/css/custom.css" rel="stylesheet">

<style media="screen">
  .cbp-spmenu-push div#page-wrapper {
    margin:0;
  }
</style>

</head>
<body class="cbp-spmenu-push">
	<div class="main-content">
		<div id="page-wrapper">
        <div class="main-page login-page ">
          <h2 class="title1">Login</h2>
          <div class="widget-shadow">
            <div class="login-body">
              <form action="<?= base_url() ?>admin/users/login" method="POST">
                <input type="text" class="user" name="username" placeholder="Enter Your Username" required="">
                <input type="password" name="password" class="lock" placeholder="Password" required="">

                <input type="submit" name="Sign In" value="Sign In">

                <?php if($this->session->flashdata('error')) : ?>
                  <center><span id="helpBlock2" class="help-block"><?= $this->session->flashdata('error') ?></span></center>
                <?php endif; ?>
              </form>
            </div>
          </div>
        </div>
<?php $this->load->view('admin/elements/footer'); ?>
